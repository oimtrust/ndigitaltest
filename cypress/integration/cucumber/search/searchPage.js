/// <reference types="Cypress" />

const URL           = 'https://www.google.com'
const KEYWORD_INPUT = 'input[name="q"]'
const SEARCH_BUTTON = 'input[name="btnK"]'

class SearchPage {
    static visit() {
        cy.visit(URL)
    }

    static typeKeyword(keyword) {
        cy.get(KEYWORD_INPUT).type(keyword)
    }

    static submit() {
        cy.get(SEARCH_BUTTON).click({multiple: true, force: true})
    }
}

export default SearchPage
