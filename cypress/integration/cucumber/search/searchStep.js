import {Given, When, Then} from 'cypress-cucumber-preprocessor/steps';
import SearchPage from './searchPage';

Given('I open google page', () => {
    SearchPage.visit()
})

When('I typing {string} in google search', keyword => {
    SearchPage.typeKeyword(keyword)
})

When('I click on Google Search button', () => {
    SearchPage.submit()
})

Then('I should see list of content', () => {
    cy.get('[href="https://id.wikipedia.org/wiki/Pikachu"] > .LC20lb > span').should('contain.text', 'Pikachu - Wikipedia bahasa Indonesia, ensiklopedia bebas')
})