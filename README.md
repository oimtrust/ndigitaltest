## How to Install
1. Clone repository
    ```
    git clone https://oimtrust@bitbucket.org/oimtrust/ndigitaltest.git
    ```
2. Install Dependencies
    ```
    npm install
    ```
3. Run cypress using GUI
    ```
    npm run cy:open
    ```
4. Run cypress using Headless Browser
    ```
    npm run cy:run
    ```